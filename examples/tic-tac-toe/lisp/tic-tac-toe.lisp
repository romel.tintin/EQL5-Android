;;;
;;; for (c) please see COPYING.txt
;;;
;;; This is a port of a QtQuick1/Qt4 example.
;;; The JS game logic has been ported to Lisp.
;;;

(in-package :eql-user)

(qrequire :quick)

(defun ini ()
  (qml:ini-quick-view "qml/tic-tac-toe.qml"))

;; quit app

(defun eql::back-pressed () ; called from QML
  (qquit))
