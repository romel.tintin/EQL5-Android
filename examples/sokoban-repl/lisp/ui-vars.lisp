;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*board*
   #:*buttons1*
   #:*buttons2*
   #:*down*
   #:*file-browser*
   #:*flick-output*
   #:*folder-model*
   #:*folder-view*
   #:*history-back*
   #:*history-forward*
   #:*left*
   #:*level*
   #:*main*
   #:*next*
   #:*path*
   #:*previous*
   #:*progress*
   #:*repl-container*
   #:*repl-input*
   #:*repl-output*
   #:*restart*
   #:*right*
   #:*rotate-player*
   #:*solve*
   #:*undo*
   #:*up*
   #:*wiggle-box*
   #:*zoom-board-in*
   #:*zoom-board-out*))

(provide :ui-vars)

(in-package :ui)

(defparameter *down*            "down")            ; Button              "qml/sokoban.qml"
(defparameter *history-back*    "history_back")    ; Button              "qml/ext/Repl.qml"
(defparameter *history-forward* "history_forward") ; Button              "qml/ext/Repl.qml"
(defparameter *left*            "left")            ; Button              "qml/sokoban.qml"
(defparameter *next*            "next")            ; Button              "qml/sokoban.qml"
(defparameter *previous*        "previous")        ; Button              "qml/sokoban.qml"
(defparameter *restart*         "restart")         ; Button              "qml/sokoban.qml"
(defparameter *right*           "right")           ; Button              "qml/sokoban.qml"
(defparameter *solve*           "solve")           ; Button              "qml/sokoban.qml"
(defparameter *undo*            "undo")            ; Button              "qml/sokoban.qml"
(defparameter *up*              "up")              ; Button              "qml/sokoban.qml"
(defparameter *repl-container*  "repl_container")  ; Column              "qml/ext/Repl.qml"
(defparameter *file-browser*    "file_browser")    ; FileBrowser         "qml/ext/FileBrowser.qml"
(defparameter *flick-output*    "flick_output")    ; Flickable           "qml/ext/Repl.qml"
(defparameter *folder-model*    "folder_model")    ; FolderListModel     "qml/ext/FileBrowser.qml"
(defparameter *folder-view*     "folder_view")     ; ListView            "qml/ext/FileBrowser.qml"
(defparameter *progress*        "progress")        ; ProgressBar         "qml/ext/Repl.qml"
(defparameter *board*           "board")           ; Rectangle           "qml/sokoban.qml"
(defparameter *rotate-player*   "rotate_player")   ; RotationAnimation   "qml/items/player.qml"
(defparameter *buttons1*        "buttons1")        ; Row                 "qml/sokoban.qml"
(defparameter *buttons2*        "buttons2")        ; Row                 "qml/sokoban.qml"
(defparameter *zoom-board-in*   "zoom_board_in")   ; ScaleAnimator       "qml/sokoban.qml"
(defparameter *zoom-board-out*  "zoom_board_out")  ; ScaleAnimator       "qml/sokoban.qml"
(defparameter *wiggle-box*      "wiggle_box")      ; SequentialAnimation "qml/items/box2.qml"
(defparameter *level*           "level")           ; Slider              "qml/sokoban.qml"
(defparameter *main*            "main")            ; StackView           "qml/sokoban.qml"
(defparameter *repl-output*     "repl_output")     ; TextEdit            "qml/ext/Repl.qml"
(defparameter *path*            "path")            ; TextField           "qml/ext/FileBrowser.qml"
(defparameter *repl-input*      "repl_input")      ; TextField           "qml/ext/Repl.qml"
