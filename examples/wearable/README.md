### Requirements

Either WearOS 2 (lots of watches) or WearOS 3 (currently Samsung Galaxy Watch 4
only). The latter is certainly the best android watch experience currently
available (WearOS 3 will come to other vendors only mid 2022, as an upgrade for
2021 watches).



### Prepare

Enable the developer options on your watch, and enable debugging over WiFi, and
connect it to your local WiFi.



### Build it

```
  ./1-copy-libs.sh

  ecl-android-32 --shell make.lisp
  qmake-android-32 wearable.pro
  make
```

Edit `connect.sh` and substitute `?` with the last number of your watch IP (as
shown in the WiFi connection menu on the watch).

```
  ./connect.sh

  ./2-build-apk.sh
```

You'll need to confirm the connection request on your watch.

If the connection script doesn't work the first time (or during deployment, see
below), just try again, or run `adb kill-server` prior to connect. You may also
(sometimes) need to turn off/on your WiFi on the watch.

The first time you run `2-build-apk.sh` it may take up to several minutes to
deploy your app to the watch through WiFi (just try to be patient...).



### Usage

Since swiping the app to the right will close the app, we use swiping up and
down to switch between the 3 pages of this app.

Page 3 is for running Swank and reloading Lisp and QML files (see examples
**my** and **REPL**).

Starting Swank (especially the first time) will take a while; after starting,
it will show the IP to connect to from the PC.

You need to complete the IP of your PC WiFi address (see 'tumbler' widget) and
run `./web-server.sh` in order to be able to load Lisp files from the PC (see
`load*`) and reload QML files (see command `:r`).



### Desktop

To run it on the desktop, do (font sizes will be different):
```
  eql5 run.lisp
```

To develop on the desktop, run the following, then connect from Slime:
```
  eql5 ~/slime/eql-start-swank.lisp run.lisp
```


### Known issues

After starting Swank, you may experience that the app doesn't close correctly,
if swiped to the right; in this case you can safely close the app from Slime
using `(eql:qquit)` (or simply `(qq)`).



### Tips

In case you don't know already: after connecting via WiFi (see `connect.sh`),
you can use `adb shell` as usual (e.g. `top` to see the memory and CPU
consumption of your app).

--

If you want to transfer data output from your app to your desktop computer (say
a log file), do:

* first ensure your app has 'storage' permission (can be granted manually in
settings)

```
  adb shell                             # after running './connect.sh'

  run-as org.qtproject.example.wearable # see 'android-sources/AndroidManifest.xml'
  cd files
  cp <file> /sdcard/
  exit                                  # exit 'run-as'

  exit                                  # exit 'adb shell'

  adb pull /sdcard/<file>               # from desktop shell
```
