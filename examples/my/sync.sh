# make clean tmp dir
adb shell rm -fr /sdcard/_my_tmp_
adb shell mkdir /sdcard/_my_tmp_

# copy desktop files to android internal storage
adb push lisp /sdcard/_my_tmp_/
adb push qml  /sdcard/_my_tmp_/

# copy files from android internal storage to android app files directory
adb shell run-as org.qtproject.example.my rm -fr /data/user/0/org.qtproject.example.my/files/lisp
adb shell run-as org.qtproject.example.my rm -fr /data/user/0/org.qtproject.example.my/files/qml
adb shell run-as org.qtproject.example.my cp -r /sdcard/_my_tmp_/lisp /data/user/0/org.qtproject.example.my/files/
adb shell run-as org.qtproject.example.my cp -r /sdcard/_my_tmp_/qml  /data/user/0/org.qtproject.example.my/files/

# clean tmp dir
adb shell rm -fr /sdcard/_my_tmp_
