
### Info

Explaining this example in one sentence could be:

#### Sketching an android app without installing the (gigantic) dev tools

(see also [mock-up android apps without installing dev tools](https://www.lights-of-holiness.eu/cl-repl/blog.htm))

So, this is just a basic skeleton for interactively developing android apps:
it's perfect for playing around, without installing some GB of development
tools first...

If you want to **use the extreme minimum** (that is, **not** installing any of
NDK, SDK, JDK, ECL, Qt5, EQL5-Android - except the sources of this example),
you can just use the
[CL myApp](https://www.dropbox.com/s/ra0ll88voc1yacg/CL%20myApp.apk?dl=0) APK
of this example, plus the standalone
[adb](https://developer.android.com/studio/releases/platform-tools.html)
command line tool for android, and Emacs/Slime on your PC, plus a python 3
installation (or any trivial web server, see script `web-server.sh`).

* connect your device via USB

* run `$ adb forward tcp:4005 tcp:4005` (port of Swank)

* run `$ adb reverse tcp:8080 tcp:8080` (port of web server; requires **android 5**)

* in this directory run `$ ./web-server.sh` or `$ web-server.bat` (requires python 3)

Now start **CL myApp** on android and tap on the REPL switch.

* on the android REPL, eval `:s` (to start Swank)

* on the PC, connect from Slime `M-x slime-connect RET RET`

Now use `lisp/my.lisp` and `qml/my.qml` for development.

* to load Lisp files located on the desktop computer, use `load*` (instead of
`load`)

example:
```
  EQL-USER> (load* "lisp/my") ; file ending defaults to ".lisp"
```
* to reload the QML file (after saving the changes) eval `:r` on the PC

![screenshot Emacs/Slime](../../screenshots/sketching.png)

The best QML editor available is **Qt Creator**, although the above QML window
uses Emacs and [qml-mode](https://www.emacswiki.org/emacs/qml-mode.el).
In QML you may (of course) include/add other QML files (or images/fonts/etc.); just
use local path names for them.

**Note**: Sometimes the Slime connection doesn't work the *first time* you try
to connect; just try to connect a second time; if this doesn't work, you
generally need to restart the android app, or sometimes even Emacs.



## BUILD APK

This obviously needs a full installation of all tools.

### Make (cross-compile)

N.B: Always run script `clean-lisp-files.sh` after changing your ECL version!

**64 bit**

```
  ./1-copy-libs.sh               # copy EQL5, ECL, prebuilt ECL libs (ASDF...)

  ecl-android --shell make.lisp
  qmake-android my.pro
  make
```

**32 bit**

```
  ./1-copy-libs-32.sh              # copy EQL5, ECL, prebuilt ECL libs (ASDF...)

  ecl-android-32 --shell make.lisp # note '32'
  qmake-android-32 my32.pro        # note '32' (2x)
  make
```

To force recompilation of all files (e.g. after upgrading ECL), pass `-f`
as last argument to the `ecl-android` command.

If you get strange compile errors from Qt5, just run this before `qmake...`:

```
  rm -fr tmp
  rm Makefile*.*
```



### Build APK file

**64 bit**
```
  ./2-build-apk.sh
```

**32 bit**
```
  ./2-build-apk-32.sh
```

This will build the APK, which needs to be copied and installed manually
(the reason is that the `--install` option would uninstall all app files,
including eventual previously installed Quicklisp libs etc.).

In order to set the app icons (3 different resolutions) and the app name
you need to run:

```
  $ qtcreator android-sources/AndroidManifest.xml
```



### APK size

If you wonder why the APK of this example is bigger than the other ones: that's
because it contains all available **Qt5 modules** from EQL5-Android, and also
many of the **QML modules** available (for playing around).



### Deploying to Play Store

In order to deploy **both 32 and 64** bit versions of your APK, you need to
include these snippets according to the target platform:

```
.snip.build.gradle.32

.snip.build.gradle.64
```

Add them in `android-build/build.gradle`.

**N.B:** Another very important tip: if you use Qt 5.13 (like here), you
need to set a **different** "Minimum required SDK" (as shown in Qt Creator,
opening `AndroidManifest.xml`) for 32 and for 64 bit.

E.g. use **API 21** for 32 bit and **API 22** for 64 bit. This is not
technically needed, but required by the Play Store, otherwise one APK would
shadow the other one.

The above is only necessary because Qt 5.13 has a minimum target API of 21
for both 32 and 64 bit. Older Qt versions have different minimum API levels
for both bit versions (so the problem doesn't exist with older Qt versions).



### Desktop notes

To run it on the desktop, do:
```
  eql5 run.lisp
```

Using Slime (see docu in desktop EQL5), first run
```
  eql5 ~/slime/eql-start-swank.lisp run.lisp
```
then connect from Emacs `M-x slime-connect`



### ASDF

The new integration (added 2020) should work for all ASDF systems supported
on mobile. (I tested successfully with e.g. `:ironclad`.)

For every dependency of your ASDF system (see `my.asd`), you should add a line
in `dependencies.lisp` how to load it (usually via Quicklisp).

If some parts of your app depend on ECL contribs, like `:sockets`, you need
to build a `*.fas` file for them (see below), and load it only after program
startup (that is, after function `copy-asset-files` from `lisp/ini.lisp` has
been called). Otherwise the parts depending on ECL contribs will fail to load.

So, if you want to build an additional `*.fas` file, see `libs.asd` and
`dependencies-libs.lisp` (e.g. for building a library to be loaded after
program startup, that is, when all ECL assets have already been copied to the
app dir, so you can require e.g. `:sockets` etc),

For building the `*.fas` file, run:
```
  ecl-android --shell make-libs.lisp
```
Now you should find the resulting file `android-sources/assets/lib/libs.fas`.
It can then be loaded (*after* program startup, in order to have the ECL
contribs available) with a trivial:
```
  (load "libs")
```
Please note that you need to `require` the used ECL contribs before loading.
In the case of `:drakma`, this would be (the OpenSSL libs need to be provided
by the app and loaded manually):
```
  (require :sockets)
  (require :asdf)

  (ffi:load-foreign-library "libcrypto.so")
  (ffi:load-foreign-library "libssl.so")

  (load "libs")
```


### Internationalization (i18n)

The basics of translations are aleady covered by the docu in EQL5 desktop
`~/eql5/doc/QtLinguist.htm`; if you are already familiar with it, the below
description should suffice:

* in Lisp, wrap every string to be translated in macro `eql:tr`
* in QML, wrap every string to be translated in `qsTr()`
```
  (print (tr "Lisp string"))

  text: qsTr("QML string")
```

Make sure you load `tr.lisp` before compiling (here: in `make.lisp`):
```
  (load "tr.lisp")
```
In order to collect all Lisp strings, you need a clean recompile of all Lisp
files. This will create a dummy file for the translations called `tr.h`.

Now you need to create/update your translation file(s). This is done by
simply running script
```
  ./my-lupdate`
```
The above generated file (e.g. `my_es.ts`) can now be opened in Qt
**linguist**.

After translating, the last step is to run:
```
  ./my-lrelease
```
This will create e.g. `my_es.qm`, ready to be included in the resource file
(see `my.qrc`).

In `build/main.cpp` you can find an example how to automatically load the
translation file for the user defined locale of the mobile device.
