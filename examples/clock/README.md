
### Introduction

This uses a `PaintedItem`, available with `import EQL5 1.0`.

Note that if you use a `PaintedItem` (for custom painting), you need to
remember:

 * use a `QQuickWidget` instead of a `QQuickView` (see [qml-lisp](lisp/qml-lisp.lisp))
 * define an initial `width` and `height` in your QML main item
 * performance and animations will not be as smooth as with `QQuickView`

The above means that it's not really meant for mobile, except if you don't use
animations at all.



### Prepare

You need to prepare your Android development environment first, as described
in [README-PREPARE](../../README-PREPARE.md)

(see also [known issues](http://wiki.qt.io/Qt_for_Android_known_issues))



### Notes

This examples assumes you already tried `../tic-tac-toe`.

As you can see from the zoom animation, it's generally not a good idea to use
animations on a `PaintedItem` (not smooth enough). Also, using a `QQuickWidget`
instead of a `QQuickView` will affect any animation negatively.

In the end you're better off just using canvas functions and JS in QML. You
can still prepare/calculate e.g. a vector of values in Lisp, and just pass it
to a trivial JS paint function in QML.



### Make (cross-compile)

```
  ./1-copy-libs.sh             # copy EQL5 and ECL libs

  ecl-android -shell make.lisp # note 'ecl-android'
  qmake-android clock.pro      # note 'qmake-android'
  make
```



### Build APK file

```
  ./2-build-apk.sh
```



### Desktop notes

To run it on the desktop, do:
```
  eql5 run.lisp
```

Using Slime (see docu in desktop EQL5), first run
```
  eql5 ~/slime/eql-start-swank.lisp run.lisp
```
then connect from Emacs `M-x slime-connect`
