;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*board*
   #:*board-image*
   #:*display*
   #:*game*
   #:*message-display*))

(provide :ui-vars)

(in-package :ui)

(defparameter *display*         "display")         ; Column    "qml/tic-tac-toe.qml"
(defparameter *board*           "board")           ; Grid      "qml/tic-tac-toe.qml"
(defparameter *board-image*     "board_image")     ; Image     "qml/tic-tac-toe.qml"
(defparameter *game*            "game")            ; Rectangle "qml/tic-tac-toe.qml"
(defparameter *message-display* "message_display") ; Text      "qml/tic-tac-toe.qml"
